﻿namespace Core.Constants
{
    public class ClaimTypes
    {
        public const string Id = "id";
        public const string Email = "email";
        public const string Role = "role";
    }
}
