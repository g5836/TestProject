﻿namespace Core.Interfaces
{
    public interface IJwtConfiguration
    {
        string GetSecretKey();

        double GetExpiredInMinutes();
    }
}
