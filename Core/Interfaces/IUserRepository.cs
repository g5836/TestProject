﻿using Core.Entities;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IUserRepository : IAsyncRepository<User>
    {
        Task<User> GetUserByEmail(string email);
    }
}
