﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ISnippetService
    {
        Task<Snippet> GetSnippetById(Guid id);

        Task<IEnumerable<Snippet>> GetSnippetsAsync(
            string title,
            int page,
            int itemsPerPage
            );

        Task<int> CountAsync(string title);

        Task<Snippet> AddSnippetAsync(
            string title,
            string textSnippet);

        Task UpdateSnippetAsync(
            Snippet snippet,
            string title,
            string textSnippet);

        Task DeleteSnippetAsync(Snippet snippet);
    }
}
