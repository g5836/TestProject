﻿using Core.Entities;

namespace Core.Interfaces
{
    public interface ISnippetRepository : IAsyncRepository<Snippet>
    {
    }
}
