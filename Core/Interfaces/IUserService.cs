﻿using Core.Entities;
using System;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IUserService
    {
        Task<User> GetUserByIdAsync(Guid id);

        Task<User> GetUserByEmailAsync(string email);

        Task<User> AddUserAsync(string email, string password, string role);
    }
}
