﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Core.Extensions
{
    public static class JsonExtensions
    {
        private static readonly JsonSerializerOptions _options = BuildOptions();

        public static T FromJson<T>(this string json) =>
            JsonSerializer.Deserialize<T>(json, _options);

        public static string ToJson<T>(this T obj) =>
            JsonSerializer.Serialize(obj, _options);

        private static JsonSerializerOptions BuildOptions()
        {
            var options = new JsonSerializerOptions();

            options.Converters.Add(new JsonStringEnumConverter());

            options.PropertyNameCaseInsensitive = true;
            options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;

            return options;
        }

        public static JsonSerializerOptions GetJsonSerializerOptions() => _options;
    }
}
