﻿using System;

namespace Core.Entities
{
    public abstract class BaseEntity
    {
        public virtual Guid Id { get; protected set; }
    }
}
