﻿using Ardalis.GuardClauses;

namespace Core.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; private set; }

        public string Password { get; private set; }

        public string Role { get; private set; }

        public string Token { get; private set; }

        public User(string email, string password, string role)
        {
            Email = email;
            Password = password;
            Role = role;
        }

        public void SetToken(string token)
        {
            Token = token;
        }

        public void UpdateDetails(string email, string password, string role)
        {
            Guard.Against.NullOrEmpty(email, nameof(email));
            Guard.Against.NullOrEmpty(password, nameof(password));
            Guard.Against.NullOrEmpty(role, nameof(role));

            Email = email;
            Password = password;
            Role = role;
        }
    }
}
