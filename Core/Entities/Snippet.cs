﻿using Ardalis.GuardClauses;

namespace Core.Entities
{
    public class Snippet : BaseEntity
    {
        public string Title { get; private set; }
        public string TextSnippet { get; private set; }

        public Snippet()
        {
        }

        public void SetTitle(string title)
        {
            Guard.Against.NullOrEmpty(title, nameof(title));
            Title = title;
        }

        public void SetTextSnippet(string textSnippet)
        {
            TextSnippet = textSnippet;
        }
    }
}
