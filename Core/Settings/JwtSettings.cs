﻿namespace Core.Settings
{
    public class JwtSettings
    {
        public string Secret { get; set; }

        public double ExpiredInMinutes { get; set; }
    }
}
