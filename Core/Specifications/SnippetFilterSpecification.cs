﻿using Ardalis.Specification;
using Core.Entities;
using System.Linq;

namespace Core.Specifications
{
    /// <summary>
    /// Please refer to the link: https://ardalis.github.io/Specification/ for more info about Specification pattern
    /// </summary>
    public class SnippetFilterSpecification : Specification<Snippet>
    {
        public SnippetFilterSpecification(string title)
        {
            Query.Where(s => s.Title.Contains(title));
        }
    }
}
