﻿using Ardalis.Specification;
using Core.Entities;
using System.Linq;

namespace Core.Specifications
{
    /// <summary>
    /// Please refer to the link: https://ardalis.github.io/Specification/ for more info about Specification pattern
    /// </summary>
    public class SnippetFilterPaginatedSpecification : Specification<Snippet>
    {
        public SnippetFilterPaginatedSpecification(string title, int skip, int take)

        {
            Query
                .Where(s => s.Title.Contains(title))
                .OrderBy(s => s.Title)
                .Skip(skip).Take(take);
        }
    }
}
