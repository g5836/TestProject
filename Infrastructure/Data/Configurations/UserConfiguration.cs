﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations
{
    internal class UserConfiguration : AbstractConfiguration<User>, IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            ConfigureKeys(builder);
            ConfigureProperties(builder);
            ConfigureRelationships(builder);
        }

        protected override void ConfigureKeys(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.Id)
               .IsRequired();

            builder.Property(u => u.Email)
               .IsRequired();

            builder.Property(u => u.Password)
             .IsRequired();

            builder.Property(u => u.Role)
             .IsRequired();

            builder.Ignore(u => u.Token);
        }

        protected override void ConfigureRelationships(EntityTypeBuilder<User> builder)
        {
        }
    }
}
