﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations
{
    internal class SnippetConfiguration : AbstractConfiguration<Snippet>, IEntityTypeConfiguration<Snippet>
    {
        public void Configure(EntityTypeBuilder<Snippet> builder)
        {
            ConfigureKeys(builder);
            ConfigureProperties(builder);
            ConfigureRelationships(builder);
        }

        protected override void ConfigureKeys(EntityTypeBuilder<Snippet> builder)
        {
            builder.HasKey(ci => ci.Id);
        }

        protected override void ConfigureProperties(EntityTypeBuilder<Snippet> builder)
        {
            builder.Property(ci => ci.Id)
             .IsRequired();

            builder.Property(ci => ci.Title)
                .IsRequired(true)
                .HasMaxLength(250);
        }

        protected override void ConfigureRelationships(EntityTypeBuilder<Snippet> builder)
        {
        }
    }
}
