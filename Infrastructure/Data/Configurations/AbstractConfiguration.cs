﻿using Core.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations
{
    public abstract class AbstractConfiguration<TEntity> where TEntity : BaseEntity
    {
        protected abstract void ConfigureKeys(EntityTypeBuilder<TEntity> builder);

        protected abstract void ConfigureProperties(EntityTypeBuilder<TEntity> builder);

        protected abstract void ConfigureRelationships(EntityTypeBuilder<TEntity> builder);
    }
}
