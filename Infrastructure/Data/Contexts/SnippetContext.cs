﻿using Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructure.Data.Contexts
{
    public class SnippetContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public SnippetContext(DbContextOptions<SnippetContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Add congiruration for all entities which implemented IEntityTypeConfiguration
            // within an assembly
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
