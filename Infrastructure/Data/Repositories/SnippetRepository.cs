﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data.Contexts;

namespace Infrastructure.Data.Repositories
{
    public class SnippetRepository : EfRepository<Snippet>, ISnippetRepository
    {
        public SnippetRepository(SnippetContext dbContext) : base(dbContext)
        {
        }
    }
}
