﻿using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data.Repositories
{
    public class UserRepository : EfRepository<User>, IUserRepository
    {
        public UserRepository(SnippetContext dbContext) : base(dbContext)
        {
        }

        public Task<User> GetUserByEmail(string email)
        {
            return _dbContext.Users
                  .Where(u => u.Email.ToLower().Equals(email.ToLower()))
                  .FirstOrDefaultAsync();
        }
    }
}
