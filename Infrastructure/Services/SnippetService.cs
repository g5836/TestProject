﻿using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class SnippetService : ISnippetService
    {
        private readonly ISnippetRepository _snippetRepository;

        public SnippetService(ISnippetRepository snippetRepository)
        {
            _snippetRepository = snippetRepository;
        }

        public async Task<Snippet> AddSnippetAsync(
            string title,
            string textSnippet)
        {
            var item = new Snippet();

            item.SetTitle(title);
            item.SetTextSnippet(textSnippet);

            return await _snippetRepository.AddAsync(item);
        }

        public async Task DeleteSnippetAsync(Snippet snippet)
        {
            await _snippetRepository.DeleteAsync(snippet);
        }

        public async Task<Snippet> GetSnippetById(Guid id)
        {
            return await _snippetRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<Snippet>> GetSnippetsAsync(
            string title,
            int page,
            int itemsPerPage)
        {
            var skip = page * itemsPerPage;
            var take = itemsPerPage;

            var spec = new SnippetFilterPaginatedSpecification(title, skip, take);

            return await _snippetRepository.ListAsync(spec);
        }

        public async Task UpdateSnippetAsync(
            Snippet snippet,
            string title,
            string textSnippet)
        {
            if (!string.IsNullOrEmpty(title))
            {
                snippet.SetTitle(title);
            }

            snippet.SetTextSnippet(textSnippet);

            await _snippetRepository.UpdateAsync(snippet);
        }

        public async Task<int> CountAsync(string title)
        {
            var spec = new SnippetFilterSpecification(title);

            return await _snippetRepository.CountAsync(spec);
        }
    }
}
