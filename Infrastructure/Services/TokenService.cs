﻿using Core.Entities;
using Core.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Constants = Core.Constants;

namespace Infrastructure.Services
{
    public class TokenService : ITokenService
    {
        private readonly IJwtConfiguration _jwtConfiguration;

        public TokenService(IJwtConfiguration jwtConfiguration)
        {
            _jwtConfiguration = jwtConfiguration;
        }

        public int ExtractUserId(string bearerToken)
        {
            var token = bearerToken.Split(" ").LastOrDefault();

            if (string.IsNullOrEmpty(token))
            {
                var handler = new JwtSecurityTokenHandler();
                var jwtSecurityToken = handler.ReadJwtToken(token);
                var claim = jwtSecurityToken.Claims.FirstOrDefault(claim => claim.Type == Constants.ClaimTypes.Id);

                if (claim != null)
                {
                    return int.Parse(claim.Value);
                }
            }

            throw new Exception("Invalid user");
        }

        public string GenerateToken(User user)
        {
            var claims = new List<Claim> {
                new Claim(Constants.ClaimTypes.Id,user.Id.ToString()),
                new Claim(Constants.ClaimTypes.Email,user.Email ),
                new Claim(Constants.ClaimTypes.Role, user.Role)
            };

            var secret = _jwtConfiguration.GetSecretKey();
            var expired = _jwtConfiguration.GetExpiredInMinutes();

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var tokenDescriptor = new JwtSecurityToken("", "", claims,
                //expires: DateTime.Now.AddMinutes(expired),
                expires: DateTime.Now.AddYears(10), // For test
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);
        }
    }
}
