﻿using Core.Interfaces;
using Core.Settings;

namespace Infrastructure.Services
{
    public class JwtConfiguration : IJwtConfiguration
    {
        private readonly JwtSettings _jwtSettings;

        public JwtConfiguration(JwtSettings jwtSettings)
        {
            _jwtSettings = jwtSettings;
        }

        public double GetExpiredInMinutes()
        {
            return _jwtSettings.ExpiredInMinutes;
        }

        public string GetSecretKey()
        {
            return _jwtSettings.Secret;
        }
    }
}
