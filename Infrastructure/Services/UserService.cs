﻿using Ardalis.GuardClauses;
using Core.Entities;
using Core.Interfaces;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> AddUserAsync(string email, string password, string role)
        {
            Guard.Against.NullOrEmpty(email, nameof(email));

            Guard.Against.NullOrEmpty(password, nameof(password));

            Guard.Against.NullOrEmpty(role, nameof(role));

            return await _userRepository.AddAsync(new User(email, password, role));
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            Guard.Against.NullOrEmpty(email, nameof(email));

            return await _userRepository.GetUserByEmail(email);
        }

        public async Task<User> GetUserByIdAsync(Guid id)
        {
            return await _userRepository.GetByIdAsync(id);
        }
    }
}
