using Core.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;
using TextSnippetApi.Configurations;
using TextSnippetApi.Configurations.AutoMapper;
using TextSnippetApi.Middlewares;

namespace TextSnippetApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddJsonOptions(opts => BuildJsonOptions(opts));// TODO : Remake with extension

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddDatabaseConfiguration(Configuration);

            services.AddAppCoreServices(Configuration);

            services.AddJwtAuthenticationConfiguration(Configuration);

            services.AddAutoMapperConfiguration();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TextSnippetApi", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseApiExceptionMiddleware();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TextSnippetApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void BuildJsonOptions(JsonOptions opts)
        {
            opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

            opts.JsonSerializerOptions.PropertyNameCaseInsensitive =
                JsonExtensions.GetJsonSerializerOptions().PropertyNameCaseInsensitive;

            opts.JsonSerializerOptions.PropertyNamingPolicy
                = JsonExtensions.GetJsonSerializerOptions().PropertyNamingPolicy;
        }
    }
}
