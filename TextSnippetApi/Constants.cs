﻿namespace TextSnippetApi
{
    public static class Constants
    {
        public const string ApiUrlTemplate = "api/v1/[controller]";

        public const int NumItemPerPage = 10;
        public const int MinPage = 1;
        public const int MinPageSize = 1;
    }

    public static class BuildEnvironments
    {
        public const string Development = "Development";
        public const string Stagging = "Stagging";
        public const string Production = "Production";
        public const string Docker = "Docker";
    }
}
