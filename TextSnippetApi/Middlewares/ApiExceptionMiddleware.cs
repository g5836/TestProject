﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading.Tasks;
using TextSnippetApi.Exceptions;

namespace TextSnippetApi.Middlewares
{
    public class ApiExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ApiExceptionMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ApiException exception)
            {
                if (httpContext.Response.HasStarted)
                {
                    //The response has already started, the http status code middleware will not be executed
                    throw;
                }

                var statusCode = HttpStatusCode.InternalServerError;

                switch (exception)
                {
                    case UnauthorizedException:
                        statusCode = HttpStatusCode.Unauthorized;
                        break;

                    case ForbiddenException:
                        statusCode = HttpStatusCode.Forbidden;
                        break;

                    case BadRequestException:
                        statusCode = HttpStatusCode.BadRequest;
                        break;

                    case NotFoundException:
                        statusCode = HttpStatusCode.NotFound;
                        break;

                    case ConflictException:
                        statusCode = HttpStatusCode.Conflict;
                        break;
                }

                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = (int)statusCode;

                // you can also use ProlemDetails
                var problem = new
                {
                    Code = (int)statusCode,
                    Message = exception.Message,
                    ErrorDetails = exception.StackTrace
                };

                await httpContext.Response.WriteAsJsonAsync(problem);

                return;
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ApiExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseApiExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ApiExceptionMiddleware>();
        }
    }
}
