﻿using System.Collections.Generic;

namespace TextSnippetApi.Models.Responses
{
    public class PaginatedList<T> where T : BaseResponse
    {
        public int CurrentPage { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int CurrentPageSize { get; set; }

        /// <summary>
        /// Total count
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// Total pages
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Has previous page
        /// </summary>
        public bool HasPreviousPage { get; set; }

        /// <summary>
        /// Has next age
        /// </summary>
        public bool HasNextPage { get; set; }

        /// <summary>
        /// Items
        /// </summary>
        public IEnumerable<T> Items { get; set; }
    }
}
