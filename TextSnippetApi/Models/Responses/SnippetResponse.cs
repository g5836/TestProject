﻿namespace TextSnippetApi.Models.Responses
{
    public class SnippetResponse : BaseResponse
    {
        public string Title { get; set; }
        public string TextSnippet { get; set; }
    }
}
