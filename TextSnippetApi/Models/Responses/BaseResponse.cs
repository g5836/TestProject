﻿using System;

namespace TextSnippetApi.Models.Responses
{
    public class BaseResponse
    {
        public Guid Id { get; set; }
    }
}
