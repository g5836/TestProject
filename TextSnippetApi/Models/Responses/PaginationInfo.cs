﻿using System.ComponentModel.DataAnnotations;

namespace TextSnippetApi.Models.Responses
{
    public class PaginationInfo
    {
        [Range(Constants.MinPage, int.MaxValue)]
        public int Page { get; set; } = Constants.MinPage;

        [Range(Constants.MinPageSize, int.MaxValue)]
        public int Size { get; set; } = Constants.NumItemPerPage;
    }
}
