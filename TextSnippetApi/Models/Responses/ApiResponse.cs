﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace TextSnippetApi.Models.Responses
{
    public class ApiResponse
    {
        public static OkObjectResult Ok(object data, string message = null)
            => new OkObjectResult(BuildResponse(HttpStatusCode.OK, data, message));

        public static CreatedResult Created(object data, string message = null)
            => new CreatedResult("", BuildResponse(HttpStatusCode.Created, data, message));

        private static object BuildResponse(HttpStatusCode httpStatusCode, object data, string message)
        {
            return new
            {
                Code = (int)httpStatusCode,
                Message = message != null ? message : httpStatusCode.ToString(),
                Data = data
            };
        }
    }
}
