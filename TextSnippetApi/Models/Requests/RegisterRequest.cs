﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;
using TextSnippetApi.DataAnnotations;

namespace TextSnippetApi.Models.Requests
{
    public class RegisterRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        [AllowedRoles(new string[] { UserRoles.User, UserRoles.Admin })]
        public string Role { get; set; }
    }
}
