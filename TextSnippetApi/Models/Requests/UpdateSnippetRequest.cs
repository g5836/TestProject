﻿using System.ComponentModel.DataAnnotations;

namespace TextSnippetApi.Models.Requests
{
    public class UpdateSnippetRequest
    {
        [Required]
        [StringLength(250, MinimumLength = 3)]
        public string Title { get; set; }

        public string TextSnippet { get; set; }
    }
}
