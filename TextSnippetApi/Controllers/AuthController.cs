﻿using AutoMapper;
using Core.Interfaces;
using Core.PasswordHasher;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TextSnippetApi.Exceptions;
using TextSnippetApi.Models.Requests;
using TextSnippetApi.Models.Responses;

namespace TextSnippetApi.Controllers
{
    [Route(Constants.ApiUrlTemplate)]
    [ApiController]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher _passwordHasher;

        public AuthController(IUserService userService, ITokenService tokenService, IMapper mapper, IPasswordHasher passwordHasher)
        {
            _userService = userService;
            _tokenService = tokenService;
            _mapper = mapper;
            _passwordHasher = passwordHasher;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="request"> A Json request body </param>
        /// <returns>User details</returns>
        /// <response code="200">Login successfully</response>
        /// <response code="401">User not found or wrong password</response>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var user = await _userService.GetUserByEmailAsync(request.Email);

            if (user == null)
            {
                throw new UnauthorizedException("Invalid email or password...!");
            }

            var passwordVerificationResult = _passwordHasher.VerifyHashedPassword(user.Password, request.Password);

            if (passwordVerificationResult == PasswordVerificationResult.Success)
            {
                var token = _tokenService.GenerateToken(user); // generate a new access token every time verification was successful

                user.SetToken(token); // set access token to the payload

                var response = _mapper.Map<UserResponse>(user);

                return ApiResponse.Ok(response);
            }

            throw new UnauthorizedException("Invalid email or password...!");
        }

        /// <summary>
        /// Register a new user.
        /// </summary>
        /// <param name="request"> A Json request body </param>
        /// <returns>A newly created user</returns>
        /// <response code="201">Create user successfully</response>
        /// <response code="409">User is already exist</response>
        [HttpPost("register")]
        public async Task<IActionResult> PostRegister([FromBody] RegisterRequest request)
        {
            var user = await _userService.GetUserByEmailAsync(request.Email);

            if (user != null)
            {
                throw new ConflictException("User is already exist");
            }

            var hashPassword = _passwordHasher.HashPassword(request.Password);

            var createdUser = await _userService.AddUserAsync(request.Email, hashPassword, request.Role);

            var token = _tokenService.GenerateToken(createdUser);

            createdUser.SetToken(token);

            var response = _mapper.Map<UserResponse>(createdUser);

            return ApiResponse.Created(response);
        }
    }
}
