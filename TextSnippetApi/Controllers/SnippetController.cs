﻿using AutoMapper;
using Core.Constants;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using TextSnippetApi.Exceptions;
using TextSnippetApi.Models.FilterModels;
using TextSnippetApi.Models.Requests;
using TextSnippetApi.Models.Responses;

namespace TextSnippetApi.Controllers
{
    [Route(Constants.ApiUrlTemplate)]
    [ApiController]
    [Authorize]
    public class SnippetController : ControllerBase
    {
        private readonly ISnippetService _snippetService;
        private readonly IMapper _mapper;

        public SnippetController(
            ISnippetService snippetService,
            IMapper mapper)
        {
            _snippetService = snippetService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get snippets by search criteria
        /// </summary>
        /// <param name="searchModel"></param>
        /// <param name="paginationInfo"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "admin, user")]
        public async Task<IActionResult> GetSnippets(
            [FromQuery] SnippetSearchModel searchModel,
            [FromQuery] PaginationInfo paginationInfo
        )
        {
            var title = searchModel.Title ?? string.Empty; // if title in payload is null, it means that there is no filter on the title
            var itemsOnPage = await _snippetService.GetSnippetsAsync(title, paginationInfo.Page - Constants.MinPage, paginationInfo.Size);

            var responseItemsOnPage = itemsOnPage.ToList().ConvertAll(snippet => MapItemToResponse(snippet));

            var totalCount = await _snippetService.CountAsync(title);

            var totalPage = int.Parse(Math.Ceiling(((decimal)totalCount / paginationInfo.Size)).ToString());

            var hasPreviousPage = paginationInfo.Page > Constants.MinPage;

            var hasNextPage = paginationInfo.Page < totalPage;

            var paginatedList = new PaginatedList<SnippetResponse>
            {
                CurrentPage = paginationInfo.Page,
                CurrentPageSize = itemsOnPage.Count(),
                TotalCount = totalCount,
                TotalPages = totalPage,
                HasPreviousPage = hasPreviousPage,
                HasNextPage = hasNextPage,
                Items = responseItemsOnPage
            };

            return ApiResponse.Ok(paginatedList);
        }

        /// <summary>
        /// Get snippet by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "admin, user")]
        public async Task<IActionResult> GetSnippetById(Guid id)
        {
            var item = await _snippetService.GetSnippetById(id);

            if (item == null)
            {
                throw new NotFoundException("Snippet not found");
            }

            var response = MapItemToResponse(item);

            return ApiResponse.Ok(response);
        }

        /// <summary>
        /// Add new Snippet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Post(AddSnippetRequest request)
        {
            var addedItem = await _snippetService.AddSnippetAsync(request.Title, request.TextSnippet);

            var response = MapItemToResponse(addedItem);

            return ApiResponse.Created(response);
        }

        /// <summary>
        /// Update existing snippet
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Put(Guid id, UpdateSnippetRequest request)
        {
            var snippet = await _snippetService.GetSnippetById(id);

            if (snippet == null)
            {
                throw new NotFoundException("Snippet not found");
            }

            await _snippetService.UpdateSnippetAsync(snippet, request.Title, request.TextSnippet);

            var response = MapItemToResponse(snippet);

            return ApiResponse.Ok(response);
        }

        /// <summary>
        /// Delete existing snippet
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.Admin)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var snippet = await _snippetService.GetSnippetById(id);

            if (snippet == null)
            {
                throw new NotFoundException("Snippet not found");
            }

            await _snippetService.DeleteSnippetAsync(snippet);

            var response = MapItemToResponse(snippet);

            return ApiResponse.Ok(response);
        }

        private SnippetResponse MapItemToResponse(Snippet item) => _mapper.Map<SnippetResponse>(item);
    }
}
