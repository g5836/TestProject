﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TextSnippetApi.DataAnnotations
{
    [System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class AllowedRolesAttribute : ValidationAttribute
    {
        private readonly string[] _roles;

        public AllowedRolesAttribute(string[] roles)
        {
            _roles = roles;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is string role)
            {
                if (!_roles.Contains(role.ToLower()))
                {
                    return new ValidationResult(GetErrorMessage());
                }
            }

            return ValidationResult.Success;
        }

        public string GetErrorMessage()
        {
            return "Role is not valid !";
        }
    }
}
