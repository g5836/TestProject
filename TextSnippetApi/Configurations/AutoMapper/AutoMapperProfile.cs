﻿using AutoMapper;
using Core.Entities;
using TextSnippetApi.Models.Responses;

namespace TextSnippetApi.Configurations.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserResponse>();
            CreateMap<Snippet, SnippetResponse>();
        }
    }
}
