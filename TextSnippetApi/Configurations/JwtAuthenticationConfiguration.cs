﻿using System.Text;
using Core.Interfaces;
using Core.Settings;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace TextSnippetApi.Configurations
{
    public static class JwtAuthenticationConfiguration
    {
        public static void AddJwtAuthenticationConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            var secret = configuration["JwtSettings:Secret"];
            var expiredString = configuration["JwtSettings:ExpiredInMinutes"];
            var expired = int.Parse(expiredString);

            var jwtConfiguration = new JwtConfiguration(new JwtSettings
            {
                Secret = secret,
                ExpiredInMinutes = expired
            });

            services.AddSingleton<IJwtConfiguration>(jwtConfiguration);

            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
        }
    }
}
