﻿using Core.Interfaces;
using Core.PasswordHasher;
using Infrastructure.Data.Repositories;
using Infrastructure.Logging;
using Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TextSnippetApi.Configurations
{
    public static class CoreServicesConfiguration
    {
        public static void AddAppCoreServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IAppLogger<>), typeof(LoggerAdapter<>));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IPasswordHasher, PasswordHasher>();

            services.AddScoped<ISnippetService, SnippetService>();
            services.AddScoped<ISnippetRepository, SnippetRepository>();
        }
    }
}
