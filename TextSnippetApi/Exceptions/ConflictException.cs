﻿using System;
using System.Runtime.Serialization;

namespace TextSnippetApi.Exceptions
{
    public class ConflictException : ApiException
    {
        public ConflictException()
        {
        }

        public ConflictException(string message) : base(message)
        {
        }

        public ConflictException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
